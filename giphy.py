import json
import giphy_client
from giphy_client.rest import ApiException
import random


def getGiphy():
    # create an instance of the API class
    api_instance = giphy_client.DefaultApi()
    api_key = '9PME55SmY87L8FICiDHjTX8CYn6kvpma'  # str | Giphy API Key.
    q = 'dog'  # str | Search query term or prhase.
    limit = 3  # int | The maximum number of records to return. (optional) (default to 25)
    offset = random.randint(0,100)  # int | An optional results offset. Defaults to 0. (optional) (default to 0)
    rating = 'g'  # str | Filters results by specified rating. (optional)
    lang = 'en'  # str | Specify default country for regional content; use a 2-letter ISO 639-1 country code. See list of supported languages <a href = \"../language-support\">here</a>. (optional)
    fmt = 'json'  # str | Used to indicate the expected response format. Default is Json. (optional) (default to json)

    try:
        # Search Endpoint
        api_response = api_instance.gifs_search_get(api_key, q, limit=limit,
                                                    offset=offset, rating=rating, lang=lang,
                                                    fmt=fmt)
        giflist = []
        for i in range(3):
            giflist.append(api_response.data[i].images.fixed_height.url)
        return giflist
    except ApiException as e:
        print("Exception when calling DefaultApi->gifs_search_get: %s\n" % e)

if __name__ == '__main__':
    getGiphy()