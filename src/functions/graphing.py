import matplotlib.pyplot as plt
from matplotlib_venn import venn2
from statistics import mean
import numpy as np
import os.path
import pandas as pd
from sklearn.decomposition import PCA as sklearnPCA
from sklearn.preprocessing import StandardScaler, robust_scale, MinMaxScaler
import seaborn as sea

def doPCA(data, controlname, wildname, replicates, ss, differentiator, columnsA, columnsB, daytime, index):
    if ss:
        cols = [F"{x}{i}" for i in columnsA for x in ['lnnsaf_', 'nlnsaf_', 'nsaf_', 'saf_', 'spc_']]
        cols2 = [F"{x}{i}" for i in columnsB for x in ['lnnsaf_', 'nlnsaf_', 'nsaf_', 'saf_', 'spc_']]
        A = str(F"{differentiator}-{controlname}")
        B = str(F"{differentiator}-{wildname}")
        folder = "/Same Same Analysis/"
        title = f"Same Same PCA \n {A} vs {B} \n Replicate combination {index+1}"
    else:
        cols = [F"{x}{i}_{controlname}" for i in range(replicates) for x in ['lnnsaf_', 'nlnsaf_', 'nsaf_', 'saf_', 'spc_']]
        cols2 = [F"{x}{i}_{wildname}" for i in range(replicates) for x in ['lnnsaf_', 'nlnsaf_', 'nsaf_', 'saf_', 'spc_']]
        A = controlname
        B = wildname
        folder = "/Comparison Data/"
        title = F" PCA \n {controlname} vs {wildname}"

    x = data[cols]
    x.columns = [i for i in range(len(x.columns))]
    for i in x.columns[1:]:
        x.loc[:, i] = MinMaxScaler().fit_transform(x[[i]])
    x2 = data[cols2]
    x2.columns = [i for i in range(len(x.columns))]
    for i in x2.columns[1:]:
        x2.loc[:, i] = MinMaxScaler().fit_transform(x2[[i]])
    df = pd.concat([x, x2])
    df.drop(df.columns[0], axis=1, inplace=True)

    targets = []
    for index, name in enumerate([controlname, wildname]):
        targets.append([F"{name}" for x in range(len(x2.iloc[:, 0].values))])
    targets = [item for l in targets for item in l]

    pca = sklearnPCA(n_components=2)
    components = pca.fit_transform(df)
    principaldf = pd.DataFrame(data=components, columns=['PC1', 'PC2'])
    principaldf['target'] = targets

    fig = plt.figure(figsize=(16, 16))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_xlabel('Principal Component 1', fontsize=30)
    ax.set_ylabel('Principal Component 2', fontsize=30)
    ax.set_title(title, fontsize=35)

    names = [str(controlname), str(wildname)]
    colours = ['green', 'purple']
    for name, colour in zip(names, colours):
        indicesToKeep = principaldf['target'] == name
        ax.scatter(principaldf.loc[indicesToKeep, 'PC1']
                   , principaldf.loc[indicesToKeep, 'PC2']
                   , c=colour
                   , s=50)

    ax.legend([[columnsA], [columnsB]])
    plt.figtext(0.5, 0.01, f"Explained variance: x: {round(pca.explained_variance_ratio_[0], 2)}, y: {round(pca.explained_variance_ratio_[1], 2)}", horizontalalignment='center', size=20) 
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    plt.axhline(0, color='red', linewidth=2)
    plt.axvline(0, color='red', linewidth=2)
    ax.grid()
    plt.savefig(daytime + folder + "PCA Protein Level " +
                A + " vs " + B + ".png")
    plt.close()


def doVolcano(data, controlname, wildname, stringency, daytime):
    fig = plt.figure(figsize=(16, 16))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_xlabel("log2 FC nsaf", fontsize=30)
    ax.set_ylabel("log10 P Value", fontsize=30)
    ax.set_title(F"Volcano Plot for \n {controlname} vs {wildname} \n {stringency}", fontsize=40)
    ax.scatter(data["fc_nsafdf"].apply(lambda x: np.log2(x)), data["ttest nsafdf"].apply(lambda x: -np.log10(x)),
               s=50)
    plt.axhline(0, color='red', linewidth=2)
    plt.axvline(0, color='red', linewidth=2)
    ax.grid()
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)

    subdirectory = "/Comparison Data/"
    newpath = os.path.join(daytime + subdirectory,
                           F"{controlname} vs {wildname} {stringency} Volcano.png")
    plt.savefig(newpath)
    plt.close()


def doHeatMap(data, controlname, wildname, rep, daytime):
    cols = [F'nsaf_{i}_{controlname}' for i in range(rep)]
    for i in range(rep):
        cols.append(F'nsaf_{i}_{wildname}')
    plot = data.sort_values(by=['fc_nsafdf'])[cols].head(20)
    plot = plot.apply(lambda x: robust_scale(np.array(x)))
    plotmax = plot[cols].max().max()
    plotmin = plot[cols].min().min()
    fig = plt.figure(figsize=(20, 20))
    ax = fig.add_subplot(1, 1, 1)
    sea.heatmap(plot, vmax=plotmax, vmin=plotmin, cmap=sea.cm.rocket_r)
    plt.xticks(rotation=90, fontsize=12)
    plt.yticks(fontsize=16)
    ax.set_xlabel("Replicate", fontsize=30)
    ax.set_ylabel("Protein ID", fontsize=30)
    plt.title("Top 20 Differentially Regulated Protein IDs", fontsize=30)
    subdirectory = "/Comparison Data/"
    newpath = os.path.join(daytime + subdirectory,
                           F"{controlname} vs {wildname} Top20 Heatmap.png")
    plt.savefig(newpath)
    plt.close()


def makeVenn(a, cn, wn, ssmode, cut, testname, daytime):
    fig = plt.figure(figsize=(16, 16))

    if ssmode == "Upregulated":
        graph = venn2([set(a.index[(a['fc_nlnsafdf'] > 1) & (a['bhtest nsafdf'] < cut)]),
                       set(a.index[(a['fc_nlnsafdf'] > 1) & (a['ttest nsafdf'] < 0.05)])],
                      [F"Same-Same BH {cut}", "P Value 0.05"])
        graph.get_patch_by_id('10').set_color('purple')
        graph.get_patch_by_id('01').set_color('pink')
        plt.title(F"{cn} vs {wn} \n Upregulated Venn Diagram", fontsize=40)
    elif ssmode == "Downregulated":
        graph = venn2([set(a.index[(a['fc_nlnsafdf'] < 1) & (a['bhtest nsafdf'] < cut)]),
                       set(a.index[(a['fc_nlnsafdf'] < 1) & (a['ttest nsafdf'] < 0.05)])],
                      [F"Same-Same BH {cut}", "P Value 0.05"])
        graph.get_patch_by_id('10').set_color('purple')
        graph.get_patch_by_id('01').set_color('pink')
        plt.title(F"{cn} vs {wn} \n Downregulated Venn Diagram", fontsize=40)
    elif ssmode == "Unchanged":
        graph = venn2([set(a.index[(a['bhtest nsafdf'] >= cut)]),
                       set(a.index[(a['ttest nsafdf'] >= 0.05)])],
                      [F"Same-Same BH {cut}", "P Value 0.05"])
        graph.get_patch_by_id('10').set_color('red')
        graph.get_patch_by_id('01').set_color('yellow')
        plt.title(F"{cn} vs {wn} \n Unchanged Venn Diagram", fontsize=40)

    for text in graph.set_labels:
        text.set_fontsize(24)
    for text in graph.subset_labels:
        text.set_fontsize(24)

    subdirectory = "/Same Same Analysis/"
    newpath = os.path.join(daytime + subdirectory,
                           F"{cn} vs {wn} Venn Diagrams {ssmode}.png")
    plt.savefig(newpath)
    plt.close()


def doVennSame(data, cn, wn, sscut, testname, daytime):
    types = ["Upregulated", "Downregulated", "Unchanged"]

    for t in types:
        makeVenn(data, cn, wn, t, sscut, testname, daytime)


def doVennComp(datacontrol, datawild, controlname, wildname, daytime, title):
    plt.figure(figsize=(16, 16))
    plt.title(F"{controlname} vs {wildname} \n {title} Venn Diagram", fontsize=30)
    graph = venn2([set(datacontrol), set(datawild)], [controlname, wildname])
    graph.get_patch_by_id('10').set_color('purple')
    graph.get_patch_by_id('01').set_color('pink')

    for text in graph.set_labels:
        text.set_fontsize(24)
    for text in graph.subset_labels:
        text.set_fontsize(24)

    subdirectory = "/Comparison Data/"
    newpath = os.path.join(daytime + subdirectory,
                           F"{controlname} vs {wildname} ~ {title} Venn Diagram.png")
    plt.savefig(newpath)
    plt.close()


def doPValHist(data, cn, wn, title, daytime):
    plt.figure(figsize=(16, 16))
    title2 = F"P Value Histogram \n {cn} vs {wn} - {title}"
    n, bins, patches = plt.hist(data, bins=20, edgecolor='black', color='yellow')
    patches[0].set_fc('g')
    plt.title(title2, fontsize=35)
    plt.xlabel('P Value Bin', fontsize=30)
    plt.ylabel('# Protein IDs', fontsize=30)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    subdirectory = "/Comparison Data/"
    newpath = os.path.join(daytime + subdirectory, f"P Value Histogram {cn} vs {wn} - {title}.png")
    plt.savefig(newpath)
    plt.close()
    plt.close()


def doSameSameHistogram(data, name, testtype, daytime):
    stringency = ["HH", "HL"]

    if testtype == "BH":

        for index, y in enumerate(stringency):

            fig, ax = plt.subplots(2, 5)
            row = 0
            for place, x in enumerate(data):
                if place > 4:
                    row = 1
                    place = place - 5
                ax[row, place].hist(x.BHData[index], bins=20, histtype='stepfilled', color='b')
            subdirectory = "/Same Same Analysis/"
            newpath = os.path.join(daytime + subdirectory, F"{name} {testtype} Histogram - {y}.png")
            plt.savefig(newpath)
            plt.close()
            plt.close()

        print("SS BH Done")

    elif testtype == "P":

        for index, y in enumerate(stringency):

            fig, ax = plt.subplots(2, 5)
            row = 0
            for place, x in enumerate(data):
                if y == "HL":
                    if place > 4:
                        row = 1
                        place = place - 5
                    ax[row, place].hist(x.HL, bins=20, histtype='stepfilled')
                else:
                    if place > 4:
                        row = 1
                        place = place - 5
                    ax[row, place].hist(x.HH, bins=20, histtype='stepfilled')
            subdirectory = "/Same Same Analysis/"
            newpath = os.path.join(daytime + subdirectory, F"{name} {testtype} Histogram - {y}.png")
            plt.savefig(newpath)
            plt.close()
            plt.close()

        print("SS P Done")


def doScoreBarplot(listofreplicates, name, col, replicatenumber, daytime):
    plt.figure(figsize=(16, 8))
    fig, ax = plt.subplots(1, replicatenumber)
    for ind, r in enumerate(listofreplicates):
        ax[ind].bar(np.array(len(r.proteinsFrame[col])), r.proteinsFrame[col])
        ax[ind].set_xlabel("ID#")
    plt.title(F"{col} plot {name}", fontsize=45)
    subdirectory = "/Data Analysis/"
    newpath = os.path.join(daytime + subdirectory, F"{col} Plot {name}.png")
    plt.savefig(newpath)
    plt.close()


def doExcel(outputtype, inputtype, data, cutval, stringency, daytime):
    if stringency == "HH":
        ostrin = 'High Stringency'
    else:
        ostrin = 'All Proteins'

    if outputtype == "Up":
        if inputtype == "TTest":
            out = data[(data['ttest nsafdf'] < cutval) & (data['fc_nsafdf'] > 1)]
            out = out.filter(
                regex="id|fc_nsafdf|fc_nlnsafdf|ttest nsafdf|bhtest nsafdf|\Anlnsaf_|\Aspc_|\Ansaf_")
            out.to_csv(daytime + "/Comparison Data/" + F"Upregulated {inputtype} - {ostrin}.csv", sep=',')
        else:
            out = data[(data['bhtest nsafdf'] < cutval) & (data['fc_nsafdf'] > 1)]
            out = out.filter(
                regex="id|fc_nsafdf|fc_nlnsafdf|ttest nsafdf|bhtest nsafdf|\Anlnsaf_|\Aspc_|\Ansaf_")
            out.to_csv(daytime + "/Comparison Data/" + F"Upregulated {inputtype} - {ostrin}.csv", sep=',')

    elif outputtype == "Down":
        if inputtype == "TTest":
            out = data[(data['ttest nsafdf'] < cutval) & (data['fc_nsafdf'] < 1)]
            out = out.filter(
                regex="id|fc_nsafdf|fc_nlnsafdf|ttest nsafdf|bhtest nsafdf|\Anlnsaf_|\Aspc_|\Ansaf_")
            out.to_csv(daytime + "/Comparison Data/" + F"Downregulated {inputtype} - {ostrin}.csv", sep=',')
        else:
            out = data[(data['bhtest nsafdf'] < cutval) & (data['fc_nsafdf'] < 1)]
            out = out.filter(
                regex="id|fc_nsafdf|fc_nlnsafdf|ttest nsafdf|bhtest nsafdf|\Anlnsaf_|\Aspc_|\Ansaf_")
            out.to_csv(daytime + "/Comparison Data/" + F"Downregulated {inputtype} - {ostrin}.csv", sep=',')

    elif outputtype == "Unchanged":
        if inputtype == "TTest":
            out = data[(data['ttest nsafdf'] >= cutval)]
            out = out.filter(
                regex="id|fc_nsafdf|fc_nlnsafdf|ttest nsafdf|bhtest nsafdf|\Anlnsaf_|\Aspc_|\Ansaf_")
            out.to_csv(daytime + "/Comparison Data/" + F"Unchanged {inputtype} - {ostrin}.csv", sep=',')
        else:
            out = data[(data['bhtest nsafdf'] >= cutval)]
            out = out.filter(
                regex="id|fc_nsafdf|fc_nlnsafdf|ttest nsafdf|bhtest nsafdf|\Anlnsaf_|\Aspc_|\Ansaf_")
            out.to_csv(daytime + "/Comparison Data/" + F"Unchanged {inputtype} - {ostrin}.csv", sep=',')

    elif outputtype == "Unique":
        data.UniqueA.dropna(how='all', axis=1).to_csv(daytime + "/Comparison Data/" + F"Unique {data.name1}.csv",
                                                      sep=',')
        data.UniqueB.dropna(how='all', axis=1).to_csv(daytime + "/Comparison Data/" + F"Unique {data.name2}.csv",
                                                      sep=',')

    elif outputtype == "DataQual":
        data.allQproteins.data.drop(['index'], axis=1).to_csv(
            daytime + "/Data Analysis/" + F"{inputtype} - All Proteins.csv", sep=',')
        data.highQproteins.data.drop(['index'], axis=1).to_csv(
            daytime + "/Data Analysis/" + F"{inputtype} - High Stringency Proteins.csv", sep=',')


def doSameSameFinalVal(data, high_pqfdr_val, low_pqfdr_val, n1, daytime):
    totalHH = []
    totalHL = []
    for i in range(len(data[0].BHData[0])):
        decoy1 = []
        decoy2 = []
        for sstest in data:
            decoy1.append(sstest.BHData[0][i])
            decoy2.append(sstest.BHData[1][i])
        totalHH.append(mean(decoy1))
        totalHL.append(mean(decoy2))

    plt.figure(figsize=(16, 16))
    plt.title(f"PQ-FDR", fontdict={'fontsize': 30, 'fontweight': 'medium'})
    fig, ax = plt.subplots(1, 2)

    ax[0].scatter([i/100 for i in range(len(totalHH))], totalHH, color='m', s=0.75)
    #ax[0].set_ylim(0, 10)
    ax[0].set_xlim(0, 1)
    ax[0].set_xlabel("Cutoff", fontsize=10)
    ax[0].set_ylabel("PQ-FDR", fontsize=10)
    ax[0].set_title(f"PQ-FDR for High Stringency \n {high_pqfdr_val}", fontdict={'fontsize': 10, 'fontweight': 'medium'})
    ax[0].axhline(y=1, linewidth=0.5, color='k')

    ax[1].scatter([i/100 for i in range(len(totalHL))], totalHL, color='r', s=0.75)
    #ax[1].set_ylim(0, 10)
    ax[1].set_xlim(0, 1)
    ax[1].set_xlabel("Cutoff", fontsize=10)
    ax[1].set_ylabel("PQ-FDR", fontsize=10)
    ax[1].set_title(f"PQ-FDR for Low Stringency \n {low_pqfdr_val}", fontdict={'fontsize': 10, 'fontweight': 'medium'})
    ax[1].axhline(y=1, linewidth=0.5, color='k')

    subdirectory = "/Same Same Analysis/"
    newpath = os.path.join(daytime + subdirectory, F"PQ-FDR Plot {n1}")
    plt.savefig(newpath)
    plt.close()
