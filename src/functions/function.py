# A function that sets up all the info needed for PepWitch to collect user data names, etc.

import glob
import pandas as pd
import numpy as np
from statistics import mean

from classes.collections import Replicate, State
from classes.data_classes import RawProtein


def GetInputName(workdir):
    filenames = 0
    control = 0
    wild = 0
    replicates = []
    dis = 0
    minspc = 5
    specfrac = 0.1
    pval = 0.05
    engine = "No"

    print("############")
    print("Welcome to PyWitch!")
    print("You can either import a single state for analysis or perform a multi-state comparison.")
    print("For a two-state comparison, please enter the control state first, then the wild type.")
    print("############")

    while filenames == 0:
        print(
            "Provide the name of the file stems (the text before the -R1.csv tail) that you want to import, separated by a space.")
        print("Example: Control Treated1 Treated2 Treated3.")
        filenames = input("Make sure that your files end in -R#.csv, but don't type that here: ").split()

        for name in filenames:
            if not glob.glob(str(workdir) + "/inputs/" + str(name) + "*.csv"):
                print("######################################")
                print("Error #1: " + str(
                    name) + " not found. The program will look for every filename provided a common stem.")
                print(
                    "So for example, if you type in 'gr', the program will pull in both 'grape.csv' and 'green.csv', etc")
                filenames = 0

        if not filenames:
            filenames = 0

        try:
            if len(filenames) != len(set(filenames)):
                print("######################################")
                print(
                    "Error #5: Files found, but you've entered the same name more than once. Why. That's silly. Don't do that.")
                filenames = 0
        except TypeError:
            continue

        try:
            if len(filenames) >= 3:
                multistatetypelock = 0
                while multistatetypelock == 0:
                    print("Do you want to conduct a {control vs multistate} analysis or an {anova-style} analysis")
                    try:
                        multistatetype = int(
                            input("[1] for multistate, [2] for anova, [3] to return to the beginning: "))
                        if int(multistatetype) == 3:
                            filenames = 0
                        if int(multistatetype) == 2:
                            filenames = 0
                        if int(multistatetype) == 1:
                            print("Which state is your control: ")
                            for i in range(len(filenames)):
                                print(str(filenames[i]) + " = " + str(i + 1))
                            controlindex = 0
                            while controlindex == 0:
                                controlstate = input()
                                try:
                                    if 1 <= int(controlstate) <= len(filenames):
                                        control = filenames[int(controlstate) - 1]
                                        wild = [filenames[x] for x in range(len(filenames)) if
                                                x != int(controlstate) - 1]
                                        print(control)
                                        print(wild)
                                        multistatetypelock = 1
                                        controlindex = 1
                                    else:
                                        print("######################################")
                                        print("Error #8: Please choose an appropriate control from the list.")
                                        controlindex = 0
                                except TypeError:
                                    print("Something is going wrong.")
                                    controlindex = 0
                        elif int(multistatetype) != 1 or 2 or 3:
                            print("######################################")
                            print("Error #9: Invalid number entered. Try again.")
                    except ValueError:
                        print("######################################")
                        print("Error #6: Invalid input. Please try again")
                        multistatetypelock = 0
                    if filenames == 0:
                        multistatetypelock = 1
        except TypeError:
            print("error")
            filenames = 0

    for x in range(len(filenames)):
        replicates.append(len(glob.glob(str(workdir) + "/inputs/" + str(filenames[x]) + "*.csv")))

    if replicates[:] != replicates[:]:
        print("######################################")
        print("Error #2: You have specified two sample states with different variable numbers: " + ''.join(
            str(replicates)).strip('[]'))
        print("The program will now exit. Please ensure that your replicate numbers are the same before retrying.")
        exit()

    replicatecarry = replicates[0]

    if len(filenames) == 2:
        control = filenames[0]
        wild = filenames[1]

    defaultvals = input("[Y/N] Do you want to use the default value set for Disregard(0)?").upper()
    lok = 0
    if defaultvals == "N":
        while lok == 0:
            try:
                dis = int(input("Disregard: "))
                lok = 1
            except ValueError:
                print("Please input a number")
    while engine != "GPM" or "PD" or "MM":
        engine = input("Engine ~ GPM, PD or MM: ")
        if engine == "GPM" or "PD" or "MM":
            break

    return filenames, control, wild, replicatecarry, dis, minspc, specfrac, pval, engine

def ImportProteinFiles(inputnames, engine, control, workdir):

        print("Importing .csv files...")

        filenames = inputnames
        controllist = []
        wildlist = []

        if engine == "GPM":
            indexes = ["Identifier", "rI", "Mr (kDa)", "log(e)"]
            datadict = {'Identifier': 'U', 'rl': 'float32', 'Mr (kDa)': 'float32',
                        'log(e)': 'float32'}  # Imports Protein Name, Spectral Count, Mol. Weight, score
            quote = ","

        elif engine == "PD":
            indexes = ["Accession", "# Peptides", "MW [kDa]", "Score"]
            datadict = {'Accession': 'U', '# Peptides': 'float32', 'MW [kDa]': 'float32',
                        'Score': 'float32'}  # Imports Protein Name, Peptides, Mol. Weight, score
            quote = '"'

        elif engine == "MM":
            indexes = ["Protein Accession", "Number of Peptides", "Protein Unmodified Mass", "Best Peptide Score"]
            quote = '"'

        for name in filenames:

            if engine != "MM":
                try:
                    try:
                        headers = pd.read_csv(str(workdir) + "/inputs/" + str(name) + "-R1.csv", nrows=1).columns.tolist()
                    except FileNotFoundError:
                        print("Stem name not properly configured")
                        exit()

                    usecol = [int(headers.index(indexes[0])), int(headers.index(indexes[1])), int(headers.index(indexes[2])),
                     int(headers.index(indexes[3]))] # Index Values
                    total = [pd.read_csv(x, delimiter = ',', index_col = False, header = 'infer',  usecols = usecol,
                                         quotechar = quote, dtype = datadict)[indexes].fillna(0.01).values.tolist()
                             for x in glob.glob(str(workdir) + "/inputs/" + str(name) + "-R*.csv")]
                except ValueError:
                    print("######################################")
                    print("Error #4: Your CSV file is empty or incompatible.")
                    exit()
            else: # If engine is MM

                try:

                    try:
                        headers = pd.read_csv(str(workdir) + "/inputs/" + str(name) + "-R1.csv", nrows=1).columns.tolist()
                    except FileNotFoundError:
                        print("Stem name not properly configured")
                        exit()

                    usecol = [int(headers.index(indexes[0])), int(headers.index(indexes[1])), int(headers.index(indexes[2])),
                     int(headers.index(indexes[3]))] # Index Values
                    totalpre = [pd.read_csv(x, delimiter = ',', index_col = False, header = 'infer',  usecols = usecol,
                                         quotechar = quote)[indexes]
                             for x in glob.glob(str(workdir) + "/inputs/" + str(name) + "-R*.csv")]

                    for replicate, _ in enumerate(totalpre):
                        for row in totalpre[replicate].index:
                            try:
                                if '|' in str(totalpre[replicate].at[row, "Protein Unmodified Mass"]):
                                    newval = np.average(list(float(i)/1000 for i in (totalpre[replicate].at[row, "Protein Unmodified Mass"].split('|'))))
                                    totalpre[replicate].at[row, "Protein Unmodified Mass"] = newval
                            except AttributeError:
                                continue
                        totalpre[replicate].loc[:, "Protein Unmodified Mass"] = pd.to_numeric(totalpre[replicate].loc[:, "Protein Unmodified Mass"]) / 1000
                        totalpre[replicate].dropna(inplace=True)

                    total = list(replicate.values.tolist() for replicate in totalpre)

                except ValueError:
                    print("######################################")
                    print(ValueError)
                    print("Error #4-MM: Your CSV file is empty or incompatible.")
                    exit()

            totalproteins = [Replicate({row[0]: RawProtein(row[0], row[1], row[2], row[3], index) for row in rep}, index) for index, rep in enumerate(total)]
            uniquenameset = set(prot for replicate in totalproteins for prot in replicate.proteins) # (for data in totaldict: for x in data: x)

            if name == control:
                workingstate = State("Control", name)
                workingstate.replicates = totalproteins
                workingstate.uniqueIDs = uniquenameset
                controllist.append(workingstate)
            if name != control:
                workingstate = State("Treatment", name)
                workingstate.replicates = totalproteins
                workingstate.uniqueIDs = uniquenameset
                wildlist.append(workingstate)

        return controllist, wildlist

def MakeBHCutoff(a, b):
    if a > 0.0 and b > 0.0:
        return mean([a, b])
    elif a > 0.0:
        return a
    else:
        return b
