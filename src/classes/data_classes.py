class ScrappyFrame:

    def __init__(self, DATA, name, type):
        self.ID = name
        self.data = DATA
        self.statetype = type


class ScrappyProtein:

    def __init__(self, id, spc, molw, score, saf, lnsaf, nsaf, nlnsaf, lnnsaf, codex):
        self.ID = id
        self.SpC = spc
        self.MolW = molw
        self.Score = score
        self.SAF = saf
        self.lnSAF = lnsaf
        self.NSAF = nsaf
        self.NlnSAF = nlnsaf
        self.lnNSAF = lnnsaf
        self.codex = codex


class RawProtein:

    def __init__(self, identifier, count, weight, score, ind):
        self.ID = identifier
        self.SpC = count
        self.MolW = weight
        self.Score = score
        self.SAF = 0.0
        self.lnSAF = 0.0
        self.NlnSAF = 0.0
        self.NSAF = 0.0
        self.lnNSAF = 0.0
        self.replicate = ind

