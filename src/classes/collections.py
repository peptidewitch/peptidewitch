import itertools
from math import log
import numpy as np
import pandas as pd
from functools import reduce
import scipy.stats as stats
from statistics import mean
from statsmodels.sandbox.stats.multicomp import multipletests

from classes.data_classes import ScrappyFrame

class TTestResults:

    def __init__(self, a, b):
        self.HH = []
        self.HL = []

        self.OnePercent = []
        self.BHData = []

        self.UniqueA = []
        self.UniqueB = []

        self.name1 = a
        self.name2 = b

        # If the ttestresult holds a scrappy result, then only HH and LL will have values stored within

    def ProduceFrame(self, a, b, ind, des, minim, dis):

        # For the All/All Data
        if ind == 1:
            try:
                df = a.data.set_index('id').join(b.data.set_index('id'), how='outer',
                                                 lsuffix=F'_{a.ID}', rsuffix=F'_{b.ID}')
                colsa = [F"spc_{i}_{a.ID}" for i in range(des)]
                colsb = [F"spc_{i}_{b.ID}" for i in range(des)]
            except AttributeError:
                df = a.set_index('id').join(b.set_index('id'), how='outer',
                                            lsuffix=F'_A', rsuffix=F'_B')
                colsa = [F"spc_{i}" for i in range(des)]
                colsb = [F"spc_{i+des}" for i in range(des)]

            newdf = df[
                ((np.count_nonzero(df[colsa].values, axis=1) > len(df[colsa].columns) - (dis + 1))
                &
                (df[colsa].sum(axis=1) >= minim))
                |
                ((np.count_nonzero(df[colsb].values, axis=1) > len(df[colsb].columns) - (dis + 1))
                 &
                 (df[colsb].sum(axis=1) >= minim))
                ]
            self.UniqueA = newdf[newdf[colsb].isna().any(axis=1)]
            self.UniqueB = newdf[newdf[colsa].isna().any(axis=1)]
            df = newdf.dropna()

        # For the H/H Data
        else:
            try:
                df = a.data.set_index('id').join(b.data.set_index('id'), how='inner',
                                                 lsuffix=F'_{a.ID}', rsuffix=F'_{b.ID}')
            except AttributeError:
                df = a.set_index('id').join(b.set_index('id'), how='inner',
                                                 lsuffix=F'_A', rsuffix=F'_B')

        #lnnsafdf = df.filter(regex="\Alnnsaf_|id")
        nlnsafdf = df.filter(regex="\Anlnsaf_|id")
        nsafdf = df.filter(regex="\Ansaf_|id")
        #lnsafdf = df.filter(regex="\Alnsaf_|id")
        #safdf = df.filter(regex="\Asaf_|id")
        #spcdf = df.filter(regex="\Aspc_|id")

        return nlnsafdf, nsafdf, df

    def DoTTest(self, con, wild, mode, combos, desired, index, minim, dis):
        ## There are 2 flavours of ttesting that we will do:
        ## 1) High vs High
        ## 2) All vs All, but at least 1 side needs to be Scrappy
        ## Comparisons will always be Control vs 1 Wild state

        if mode == "normal":
            w = con.highQproteins
            x = wild.highQproteins
            y = con.allQproteins
            z = wild.allQproteins
            mid = desired
            end = desired * 2
            combination_type = [[w, x], [y, z]]

        else:
            w = con.samesamehighQ[index]
            w2 = con.samesamehighQ[-index-1]
            x = con.samesamemegaframes[index]
            x2 = con.samesamemegaframes[-index-1]
            mid = 3
            end = 6
            combination_type = [[w, w2], [x, x2]]

        for i, combo in enumerate(combination_type):

            nlnsafdf, nsafdf, df = self.ProduceFrame(combo[0], combo[1], i, mid, minim, dis)

            df['ttest nlnsafdf'] = nlnsafdf.apply(lambda row: stats.ttest_ind(row[0:mid], row[mid:end]).pvalue, axis=1)
            df['ttest nsafdf'] = nsafdf.apply(lambda row: stats.ttest_ind(row[0:mid], row[mid:end]).pvalue, axis=1)
            df['bhtest nsafdf'] = multipletests(df['ttest nsafdf'], alpha=0.05, method='fdr_bh')[1]
            df['fc_nsafdf'] = nsafdf.apply(lambda row: row[mid:end].sum() / row[0:mid].sum(), axis=1)
            df['fc_nlnsafdf'] = nlnsafdf.apply(lambda row: row[mid:end].sum() / row[0:mid].sum(), axis=1)

            if i == 0:
                self.HH = df
            else:
                self.HL = df

    def findQ(self):

        for index, frame in enumerate([self.HH, self.HL]):
            ss = []
            lock = False
            for cutoff in range(0, 99, 1):
                cut = cutoff/100
                total = len(frame['bhtest nsafdf'])
                truevals = frame['bhtest nsafdf'] < cut
                truevals = truevals.tolist().count(True)
                ss.append(100 * (truevals / total))
                if 100 * (truevals / total) >= 1:
                    if lock == False:
                        self.OnePercent.append(cut)
                        lock = True
                    else:
                        continue
            if lock == False:
                self.OnePercent.append(0)

            self.BHData.append(ss)

        if not self.OnePercent:
            self.OnePercent = [0, 0]

        return self.OnePercent


class Replicate:

    def __init__(self, prot, ind):
        self.sumSAF = 0
        self.sumlnSAF = 0
        self.proteins = prot # This is a dictionary {protein: RawProtein}
        self.number = ind
        self.proteinsFrame = 0

    def getAllIDs(self):
        return list(self.proteins[protein].ID for protein in self.proteins)

    def calculateSAFs(self):
        counter = 0 # for sumSAF
        counter2 = 0 # for sumlnSAF
        for protein in self.proteins:
            SAF = self.proteins[protein].SpC / ((self.proteins[protein].MolW * 1000)/ 110)
            lnSAF = log(SAF)
            counter += SAF
            counter2 += lnSAF
            self.proteins[protein].SAF = SAF
            self.proteins[protein].lnSAF = lnSAF
        self.sumSAF = counter
        self.sumlnSAF = counter2
        for protein in self.proteins:
            self.proteins[protein].NSAF = self.proteins[protein].SAF / self.sumSAF
            self.proteins[protein].lnNSAF = log(self.proteins[protein].NSAF)
            self.proteins[protein].NlnSAF = self.proteins[protein].lnSAF / self.sumlnSAF

    def dicttofframe(self):

        df = pd.DataFrame(columns=['id', 'spc', 'molw', 'score', 'saf', 'lnsaf', 'nsaf', 'lnnsaf', 'nlnsaf', F'{self.number}'])

        df['id'] = [x for x in self.proteins.keys()]
        proobjlist = [x for x in self.proteins.values()]

        df['spc'] = [pro.SpC for pro in proobjlist]
        df['molw'] = [pro.MolW for pro in proobjlist]
        df['score'] = [pro.Score for pro in proobjlist]
        df['saf'] = [pro.SAF for pro in proobjlist]
        df['lnsaf'] = [pro.lnSAF for pro in proobjlist]
        df['nsaf'] = [pro.NSAF for pro in proobjlist]
        df['lnnsaf'] = [pro.lnNSAF for pro in proobjlist]
        df['nlnsaf'] = [pro.NlnSAF for pro in proobjlist]

        self.proteinsFrame = df


class State:

    def __init__(self, type, name):
        self.type = type
        self.name = name
        self.uniqueIDs = set()
        self.replicates = [] # List of all replicates, each list containing Raw Protein Objs
        self.highQproteins = True
        self.allQproteins = True
        self.repIDs = [] # Protein names only, Split into replicates

        self.megaframe = True
        self.samesamemegaframes = [] # 19 Frames For Same Same
        self.samesamehighQ = [] # High Stringency Data for SameSame

        self.samesamettestresults = [] # This is where the same same results are stored
        self.BHCutoff = 0.0
        self.HighStringencyBHCutoff = None
        self.LowStringencyBHCutoff = None
        self.AllBHData = []

    def getEveryreplicateIDs(self):
        self.repIDs = [rep.getAllIDs() for rep in self.replicates]

    def makeSameSameFrames(self, combo):
        dfs = [r.proteinsFrame.copy() for r in [self.replicates[x] for x in combo]]
        for frame, x in zip(dfs, combo):
            frame.columns += F"_{x}"
            frame.rename(columns={F"id_{x}": 'id'}, inplace=True)
        a = reduce(lambda left, right:
                   pd.merge(left, right, on='id', how='outer').fillna(0, downcast='infer'), dfs)
        a = a.reindex(sorted(a.columns), axis=1)
        self.samesamemegaframes.append(a.drop([F"{x}_{x}" for x in combo], axis=1))

    def makeMegaFrame(self, repnum):
        dfs = [r.proteinsFrame for r in self.replicates]
        new_df = dfs[0].copy()
        if repnum == 3:
            new_df.columns = [f'{name}_0' for name in new_df]
            new_df = new_df.rename(columns={'id_0': 'id', '0_0': '0'})
        for index, _ in enumerate(dfs):
            if index == len(dfs) - 1:
                break
            new_df = new_df.merge(dfs[index+1], on='id', how='outer',
                        suffixes=(f"_{index}", f"_{index+1}")).fillna(0, downcast='infer')

        new_df = new_df.reindex(sorted(new_df.columns), axis=1)
        new_df = new_df.drop([str(i) for i in range(repnum)], axis=1)

        self.megaframe = new_df

    def sortStringency(self, minim, dis, ID, ty, mode, where, combos, repnum):

        # Sort stringency will take in the megaframe with all IDs and then produce a sub-frame
        # This second frame will have high stringency data.
        # The original frame we can consider as our allQuality data (Low Stringency)
        # This also performs scrappy

        if mode == "samesame":
            cols = [F"spc_{i}" for i in combos]
            df = self.megaframe.filter(regex=F"{combos[0]}|{combos[1]}|{combos[2]}|id")
        elif mode == "normal":
            cols = [F"spc_{i}" for i in range(repnum)]
            df = self.megaframe

        a = df.loc[:, cols].sum(axis=1)
        highqdf = df[a > minim]
        highqdf = highqdf[np.count_nonzero(highqdf[cols].values, axis=1) > len(highqdf[cols].columns) - (dis + 1)]

        if mode == "samesame":
            self.samesamehighQ.append(ScrappyFrame(highqdf.reset_index(), where, ty))
        else:
            self.highQproteins = ScrappyFrame(highqdf.reset_index(), ID, ty)
            self.allQproteins = ScrappyFrame(df.reset_index(), ID, ty)

    def sameSameAnalysis(self, mini, dis, statecontrol, desire):

        combinations = [x for x in itertools.combinations([0,1,2,3,4,5], 3)]
        for index, combo in enumerate(combinations):
            print(F"...Stringency sort {index+1} of {len(combinations)}")
            self.makeSameSameFrames(combo)
            self.sortStringency(mini, dis, self.name, self.type, "samesame", index, combo, desire)
        for index, combo in enumerate(combinations):
            if index == 10:
                break
            print(F"...TTesting Combination {index+1} of {int(len(combinations)/2)}")
            newtest = TTestResults(index, 19-index)
            newtest.DoTTest(statecontrol[0], [1], "samesame", combinations, desire, index, mini, dis)
            self.samesamettestresults.append(newtest)
        allcutoffresults = []
        for index, result in enumerate(self.samesamettestresults):
            print(F"...Finding Q {index+1} of {len(self.samesamettestresults)}")
            allcutoffresults.append(result.findQ())

        HighStringencyBHCut = mean([test[0] for test in allcutoffresults])
        LowStringencyBHCut = mean([test[1] for test in allcutoffresults])
        self.BHCutoff = mean([HighStringencyBHCut, LowStringencyBHCut])
        self.HighStringencyBHCutoff = HighStringencyBHCut
        self.LowStringencyBHCutoff = LowStringencyBHCut

        for result in self.samesamettestresults:
            self.AllBHData.append(result.BHData)
