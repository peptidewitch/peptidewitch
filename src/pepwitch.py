import datetime
import itertools
import matplotlib
matplotlib.use("Agg")
import os.path
from sys import argv
import shutil
import sys

from classes.collections import TTestResults
from functions.function import *
from functions.graphing import *


def main(mode):

    #Setting up a few housekeeping items.
    np.set_printoptions(precision=2)
    WorkingDirectory = os.path.dirname(os.path.realpath(__file__))

    # Checks to see whether the user is coming through peptidewitch.com or through a local client
    # Also establishes the pathways for input and output files
    if mode == "online":
        print("We're online. You shouldn't see this, but the log will record it.")

        MinSpc = float(argv[1])
        Disregard = int(argv[2])
        SameSamePValue = float(argv[3])
        DesiredReplicateNumber = int(argv[4])
        SpectralFraction = float(argv[5])
        if argv[6] == "PD":
            Engine = "PD"
        elif argv[6] == "MM":
            Engine = "MM"
        else:
            Engine = "GPM"

        CSVInputName = argv[7:]

        Control = CSVInputName[0]
        print(Control)
        if len(CSVInputName) > 1:
            Wild = " ".join(str(x) for x in CSVInputName[1:])
            print(Wild)
    else: # If mode is offline
        CSVInputName, Control, Wild, DesiredReplicateNumber, Disregard, \
        MinSpc, SpectralFraction, SameSamePValue, Engine = GetInputName(WorkingDirectory)

    DayandTime = WorkingDirectory + "/output/" + str(
            datetime.datetime.now().strftime('%d-%m-%Y_%H-%M-%S')) + \
                 '-' + '-'.join(CSVInputName) + "-" + str(MinSpc)
    os.makedirs(DayandTime)

    ## This function imports the .csv files into our workflow and makes a list of the replicates.
    stateControl, stateWild = ImportProteinFiles(CSVInputName, Engine, Control, WorkingDirectory)

    if not stateControl:
        stateControl = stateWild
        stateWild = None
        COMPARISON = False
        os.mkdir(DayandTime + "/Data Analysis/")
    else:
        COMPARISON = True
        os.mkdir(DayandTime + "/Comparison Data/")
        os.mkdir(DayandTime + "/Data Analysis/")

    if DesiredReplicateNumber == 6:
        os.mkdir(DayandTime + "/Same Same Analysis/")

    ## Now we get the replicate objects to produce SAF values
    print("Producing SAFs")
    for state in stateControl:
        for replicate in state.replicates:
            replicate.calculateSAFs()
            replicate.dicttofframe()
    if COMPARISON:
        for state in stateWild:
            for replicate in state.replicates:
                replicate.calculateSAFs()
                replicate.dicttofframe()

    ## Time to make high stringency data by applying Scrappy Rules
    print("Producing HS Data")
    for state in stateControl:
        state.makeMegaFrame(DesiredReplicateNumber)
        state.sortStringency(MinSpc, Disregard, state.name, state.type, "normal", 0, 0, DesiredReplicateNumber)
        if len(state.replicates) == 6:
            print(F"Conducting SameSame Analysis for {state.name}")
            state.sameSameAnalysis(MinSpc, Disregard, stateControl, DesiredReplicateNumber)
    if COMPARISON:
        for state in stateWild:
            state.makeMegaFrame(DesiredReplicateNumber)
            state.sortStringency(MinSpc, Disregard, state.name, state.type, "normal", 0, 0, DesiredReplicateNumber)
            if len(state.replicates) == 6:
                print(F"Conducting SameSame Analysis for {state.name}")
                state.sameSameAnalysis(MinSpc, Disregard, stateWild, DesiredReplicateNumber)

    # Comparative testing
    if COMPARISON:
        print("Conducting Comparative Testing Between States")
        ControlvsWildTests = []
        for wild in stateWild:
            ttestresults = TTestResults(stateControl[0].name, wild.name)
            ttestresults.DoTTest(stateControl[0], wild, "normal", 0, DesiredReplicateNumber, 0, MinSpc, Disregard)
            ControlvsWildTests.append(ttestresults)
    else:
        ControlvsWildTests = None

    # Making Comparison Data Outputs
    # SameSame PCA
    if DesiredReplicateNumber == 6:
        print("Making Same Same PCA")
        combinations = [x for x in itertools.combinations([0, 1, 2, 3, 4, 5], 3)]
        for state in stateControl:
            for index, result in enumerate(state.samesamettestresults):
                doPCA(result.HL, result.name1, result.name2, 3, True,
                      state.name, combinations[index], combinations[19-index], DayandTime, index)
            doSameSameFinalVal(state.samesamettestresults, state.HighStringencyBHCutoff, state.LowStringencyBHCutoff, state.name, DayandTime)

        if COMPARISON:
            for state in stateWild:
                for index, result in enumerate(state.samesamettestresults):
                    doPCA(result.HL, result.name1, result.name2, 3, True,
                        state.name, combinations[index], combinations[19-index], DayandTime, index)
    
    if ControlvsWildTests:
        for index, result in enumerate(ControlvsWildTests):

            # This will make every same same test control vs wild specific, rather than averaging all 3 together.
            BHCut = MakeBHCutoff(stateControl[0].BHCutoff, stateWild[index].BHCutoff)

            print("Making Test PCAs")
            # PCA Replicates HH
            doPCA(result.HH, result.name1, result.name2, DesiredReplicateNumber, False, result.name2, result.name1, result.name2, DayandTime, index)
            # PCA Replicates HL
            doPCA(result.HL, result.name1, result.name2, DesiredReplicateNumber, False, result.name2, result.name1, result.name2, DayandTime, index)

            print("Making Test Volcanos")
            # Volcano HH
            doVolcano(result.HH, result.name1, result.name2, "High Stringency", DayandTime)
            # Volcano HL
            doVolcano(result.HL, result.name1, result.name2, "All Data", DayandTime)

            print("Making Heatmap")
            # Top 25 in Control HeatMap HH
            doHeatMap(result.HH, result.name1, result.name2, DesiredReplicateNumber, DayandTime)

            print("Making Venn Diagrams")
            # Venn Diagram IDs
            doVennComp(stateControl[0].uniqueIDs, stateWild[index].uniqueIDs,
                    stateControl[0].name, stateWild[index].name, DayandTime, "Low Stringency IDs")
            doVennComp(stateControl[0].highQproteins.data['id'], stateWild[index].highQproteins.data['id'],
                    stateControl[0].name, stateWild[index].name, DayandTime, "High Stringency IDs")

            print("Making P Val Histograms")
            # Make P-Val Histogram
            doPValHist(result.HH['ttest nlnsafdf'], result.name1, result.name2, "High Stringency", DayandTime)
            doPValHist(result.HL['ttest nlnsafdf'], result.name1, result.name2, "All Data", DayandTime)

            print("Making Excel outputs")
            # Excel Outputs
            for testtype in ['HH', 'HL']:
                doExcel("Up", 'TTest', getattr(result, testtype), 0.05, testtype, DayandTime)
                doExcel("Down", 'TTest', getattr(result, testtype), 0.05, testtype, DayandTime)
                doExcel("Unchanged", 'TTest', getattr(result, testtype), 0.05, testtype, DayandTime)
            doExcel("Unique", 'na', result, 0, testtype, DayandTime)

            if DesiredReplicateNumber == 6:

                print("Making Same Same Comparative Venn, Final and Excel")

                # TvsQ Venn Diagrams
                doVennSame(result.HH, result.name1, result.name2, BHCut, "Same Same Retest High Stringency", DayandTime)
                doVennSame(result.HL, result.name1, result.name2, BHCut, "Same Same Retest Low Stringency", DayandTime)
                # Same Same Final
                #doSameSameFinalVal(stateControl[0].samesamettestresults, stateControl[0].HighStringencyBHCutoff, stateControl[0].LowStringencyBHCutoff, result.name1, DayandTime)
                doSameSameFinalVal(stateWild[index].samesamettestresults, stateWild[index].HighStringencyBHCutoff, stateWild[index].LowStringencyBHCutoff, result.name2, DayandTime)
                #Excel
                for testtype in ['HH', 'HL']:
                    doExcel("Up", "Same Same", getattr(result, testtype), BHCut, testtype, DayandTime)
                    doExcel("Down", "Same Same", getattr(result, testtype), BHCut, testtype, DayandTime)
                    doExcel("Unchanged", "Same Same", getattr(result, testtype), BHCut, testtype, DayandTime)

    print("Making Control Data Analysis")
    # Making Data Analysis Outputs
    for state in stateControl:
        # Score Barplot
        # doScoreBarplot(state.replicates, state.name, 'score', DesiredReplicateNumber, DayandTime)
        # NlnSAF Barplot
        # doScoreBarplot(state.replicates, state.name, 'nlnsaf', DesiredReplicateNumber, DayandTime)
        # Excel Output HH
        doExcel("DataQual", state.name, state, 0, 0, DayandTime)
        """
        if DesiredReplicateNumber == 6:
            # SameSame P Val
            doSameSameHistogram(state.samesamettestresults, state.name, "BH", DayandTime)
            # SameSame BH
            doSameSameHistogram(state.samesamettestresults, state.name, "P", DayandTime)
        """

    if COMPARISON:
        print("Making Wild Data Analysis")
        for state in stateWild:
            # Score Barplot
            # doScoreBarplot(state.replicates, state.name, 'score', DesiredReplicateNumber, DayandTime)
            # NlnSAF Barplot
            # doScoreBarplot(state.replicates, state.name, 'nlnsaf', DesiredReplicateNumber, DayandTime)
            # Excel Output HH
            doExcel("DataQual", state.name, state, 0, 0, DayandTime)
            """
            if DesiredReplicateNumber == 6:
                print("Making Same Same Comparative Stuff")
                doSameSameHistogram(state.samesamettestresults, state.name, "BH", DayandTime)
                # SameSame BH
                doSameSameHistogram(state.samesamettestresults, state.name, "P", DayandTime)
            """

    #End Processes
    def End():
        if mode == "offline":
            shutil.make_archive(WorkingDirectory + "/Output " + str(CSVInputName), 'zip', DayandTime)
            #shutil.rmtree(DayandTime)
        if mode == "online":
            shutil.make_archive(os.path.dirname(os.path.realpath(__file__)) + "/Output", 'zip', DayandTime)
    End()


if __name__ == "__main__":
    print("Python Version: " + str(sys.version))
    print("Checking for offline mode")
    try:
        if argv[6] == "GPM" or "PD":
            main("online")
    except IndexError:
        main("offline")

