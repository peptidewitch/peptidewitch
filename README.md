PeptideWitch [v3.2019.June]
---

## Welcome

PeptideWitch is designed to be run via the front-end web iterface over at http://peptidewitch.com.
For development purposes, debugging, feature adding, or if you want to run a local version, please see the instructions below:

---

## PeptideWitch Installation
---
Step 1: Make sure you have Python installed. During installation, check the box for adding Python to PATH. [https://www.python.org/ftp/python/3.7.3/python-3.7.3-amd64.exe]

Step 2: With python all set up, open up command prompt/terminal and type `pip install pipenv`

Step 3: Download a copy of git: [https://git-scm.com/]

Step 4: Open up the git terminal and `cd` into your home folder.

Step 5: Type `git clone https://peptidewitch@bitbucket.org/peptidewitch/peptidewitch.git`

Step 6: Type `cd peptidewitch`

Step 7: Type `pipenv install`

---

## PeptideWitch Usage
---

Step 1: Transfer the files you want analysed into your folder: `/peptidewitch/src/inputs/`

Step 2: In the command prompt/terminal, `cd` to the peptidewitch folder and run `pipenv shell`. Now you're inside a contained virtual python environment that can run Witch :)

Step 3: Type `python src/PepWitch2.1.py`

Step 4: Follow the instructions in-code and you're away! 