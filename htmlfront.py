from flask import Flask, render_template, request, send_file
import os
import shutil
import sys
from giphy import getGiphy

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def home(name=None):
    if request.method == "GET":
        if os.path.isfile("Output.zip"):
            os.remove("Output.zip")
            print("File Erased")
        gif = getGiphy()
        return render_template('home.html', name=name, gif1=gif[0], gif2=gif[1], gif3=gif[2])
    if request.method == "POST":
        pathtofile = os.path.dirname(os.path.realpath(__file__))
        print("Sys version: " + sys.version)
        outputdirec = os.path.join(pathtofile, 'inputs')
        try:
            os.makedirs(outputdirec)
            print(outputdirec)
        except FileExistsError:
            print("Directory already exists")
        uploaded_files = request.files.getlist("file[]")
        names = request.form["names"]
        minspc = request.form["minspc"]
        disregard = request.form["disregard"]
        pval = request.form["pval"]
        replicates = request.form["replicate"]
        spcfrac = request.form["spcfrac"]
        engine = request.form["engine"]
        print(names, minspc, disregard, pval, replicates, spcfrac, engine)
        for file in uploaded_files:
            newpath = os.path.join(outputdirec, file.filename)
            file.save(newpath)
            file.close()
        os.chdir(pathtofile)
        os.system("python -V")
        print("Starting")
        os.system("/home/PeptideWitch/.virtualenvs/my-virtualenv/bin/python3.6 PepWitch2.1.py " +
                  str(minspc) + " " + str(disregard) + " "  + str(pval) + " " + str(replicates) + " " +
                  str(spcfrac) + " " + str(engine) + " " + str(names))
        print("Finished")
        shutil.rmtree(outputdirec)
        return send_file(pathtofile + "/Output.zip", as_attachment=True)

if __name__ == "__main__":
    app.run()
